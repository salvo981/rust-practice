/*
Examples of Strings, which are collections as in chaper 8 of Rust programming book
NOTE: Strings in Rust are complicated so this part of Chapter 8 is particularly important
*/



fn main() {
    println!("let7s play with Strings!");
    
    // Let's see different ways to create strings
    let s = String::new(); //empty string
    let data = "Minchia che stringa!"; // string literal
    let s = data.to_string();
    let s = "Directly from literals...".to_string();
    let s = String::from("initial contents");
    
    // IMPORTANT: String::from() and to_string() do exactly the same thing in this
    // what to use depends on style and readability
    
    // Strings are UTF-8 encoded so we can use any UTF-8 symbol in Strings
    let hello = String::from("Здравствуйте");
    println!("Hello in Russian:\t{hello}");
    
    // Update strings
    let mut s = String::from("Здравствуйте");
    let s2 = String::from(" means hello in russian");
    // let s2 = " means hello in russian";
    // This would append to the Strings
    s.push_str(&s2);
    println!("{s}");
    // The push method instead, add single characters to the String
    s.push('!');
    println!("{s}");
    // NOTE: s2 si still accessible as we are passing the reference to it to push_str
    // if we would pass s2 directly as a string object it would panic
    println!("{s2}");
    
    // Concatenation with the + Operator or the format! Macro
    let s1 = String::from("Hello, ");
    let s2 = String::from("world!");
    
    /*
    The reason s1 is no longer valid after the addition, and
    the reason we used a reference to s2 , has to do with the signature of the method that’s called when
    we use the + operator. The + operator uses the add method, whose signature looks something like this:
    fn add(self, s: &str) -> String {
    */
    
    /*
    let s3 = s1 + &s2 does the following:
    - takes ownership of s1
    - appends a copy of the contents of s2
    - returns ownership of the result 
    The implementation is more eﬃcient than copying.
    */
    let s3 = s1 + &s2;
    println!("s3 is: {s3}");
    // The '+' operator is cumbersome to use when multiple string need to be combined
    let s1 = String::from("tic");
    let s2 = String::from("tac");
    let s3 = String::from("toe");
    // This looks fairly complicated and difficult to understand what is going on
    let s = s1 + "-" + &s2 + "-" + &s3;
    println!("\nConcatenate many strings with the '+' operator: {s}");
    
    /*
    In such cases the format! macro is easier to use
    format! is much easier to read,
    and does not take ownership, which means that in the example below
    s1 is still usable
    */
    let s1 = String::from("tic");
    let s = format!("{s1}-{s2}={s3}");
    println!("\nConcatenate many strings with the format! macro: {s}");
    
    /*
    Indexing into Strings
    In Rust it is not allowed to access Strings through their indexes like in other languages.
    This because String are stored as vectors of bytes, Vec<u8> more specifically.
    */
    
    let s1 = String::from("hello");
    // let h = s1[0];
    
    /*
    Indexing does not work because some Unicode characters require 2 bytes to be encoded.
    for example each character in 大阪 required 2 bytes, hence the string lenght is 4 (and not 2).
    so if try to do:
    let s = "大阪";
    let h = s[0]; // we do not get any valid character
    */
    
    let s = "大阪";
    // The line below would not work!
    // let h = &s[0]; // we do not get any valid character    
 
    /*
    Iterating Over Strings
    */
    
    // The best way to iterate is decide on what to iterate (e.g., chars or bytes)
    
    // Iterate through chars.
    //  A char in UTF-8 can be 1 or 2 bytes long
    let s = String::from("大阪");
    println!("Iterate string {s} through chars:");
    for c in s.chars() {
        println!("{c}");
    }
    
    // Alternatively the raw bytes can be returned
    println!("Iterate string through bytes:");
    for b in s.bytes() {
        // In this case each Kanji requires 3 bytes to be encoded
        // hence 6 byte values are printed
        println!("{b}");
    }
    
                      
}

