/// Parse alignment file and generate aln scores for Sonicparanoid
// use std::error::Error;
// use std::fs;

use std::collections::HashMap;
use std::fmt;
use std::cmp;
use std::fs::File;
use std::io::Write;
use std::io::{BufReader, BufRead};
use std::path::Path;
use indexmap::IndexMap;
// use crate::seq_tools::load_protein_lengths;
// use std::io::ErrorKind;



/// Contains the fields of the HSP
#[derive(Debug)]
struct HspNumericFields {
    // Contains all the required info regarding an HSP, apart from query and target ids
    // which are stored in separate tuples
    qlen: u32,
    slen: u32,
    qstart: u32,
    qend: u32,
    sstart: u32,
    send: u32,
    bscore: f64, // this should be a u16 (max would be ~65K)
}


/// Contains the information about the HSPs from a query-target pair
/// these information are later used by SonicParanoid compute graph-absed orthologs
#[derive(Debug)]
struct QueryTargetPairScores {
    query_id: String,
    target_id: String,
    bscore: f64,
    query_len: u32,
    target_len: u32,
    // NOTE: tot_query_cov and tot_target_cov contain the 'coverage' of the total sequence,
    // disergarding regions of the sequences that are not matching inbetween multiple HSPs.
    tot_query_cov: u32,
    tot_target_cov: u32,
    // NOTE: query_cov and target_cov represent the actual coverages for the query and target respectively
    query_cov: u32,
    target_cov: u32,
}

impl fmt::Display for QueryTargetPairScores {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}\t{}\t{:.1}\t{}\t{}\t{}\t{}\t{}\t{}",
        self.query_id,
        self.target_id,
        self.bscore,
        self.query_len,
        self.target_len,
        self.tot_query_cov,
        self.tot_target_cov,
        self.query_cov,
        self.target_cov,
    )
    }
}


/// Contains the fields of the HSP
#[derive(Debug)]
struct HspCoordinates {
    // Contais and start and end position f an HSP
    start: u32,
    end: u32,
}



/*
// TODO: some crates are required to imporve performance of different steps
// itoa: fast integer to string conversion
// ryu: fast float to string conversion
// itertools: extra tools for iterators (e.g. enumerate, zip etc.)
*/


/// Check if there is an overlap between two HSP
// #[inline(always)]
// #[inline(never)] fn check_hsp_overlap(hsp1start: u32, hsp1end: u32, hsp2start: u32, hsp2end: u32) -> bool {
fn check_hsp_overlap(hsp1start: u32, hsp1end: u32, hsp2start: u32, hsp2end: u32) -> bool {

    // there is an overlap for sure
    if hsp1start == hsp2start {return true }

    // define variables
    const OVERLAP_THR: f32 = -0.05;
    // NOTE: we use i64 instead to allow negative difference of the integers
    let leftmost_hsp_start: i64;
    let leftmost_hsp_end: i64;
    let rightmost_hsp_start: i64;
    let rightmost_hsp_end: i64;

    // position the HSP first (leftmost or rightmost)
    // hsp1 is the leftmost fragment
    if hsp1start < hsp2start {
        leftmost_hsp_start = hsp1start as i64;
        leftmost_hsp_end = hsp1end as i64;
        rightmost_hsp_start = hsp2start as i64;
        rightmost_hsp_end = hsp2end as i64;
    }
    // hsp2 is the leftmost fragment
    else {
        leftmost_hsp_start = hsp2start as i64;
        leftmost_hsp_end = hsp2end as i64;
        rightmost_hsp_start = hsp1start as i64;
        rightmost_hsp_end = hsp1end as i64;
    }

    // calculate the lengths of the HSPs
    // let leftmost_hsp_len: i64 = leftmost_hsp_end - leftmost_hsp_start + 1;
    // let rightmost_hsp_len: i64 = rightmost_hsp_end - rightmost_hsp_start + 1;
    // find the shortest HSP
    // let shortest_hsp_len: i64 = cmp::min(leftmost_hsp_len, rightmost_hsp_len);
    let shortest_hsp_len: i64 = cmp::min(leftmost_hsp_end - leftmost_hsp_start + 1, rightmost_hsp_end - rightmost_hsp_start + 1);

    //calculate the overlap
    let overlap: f32 = (rightmost_hsp_start - leftmost_hsp_end - 1) as f32 / shortest_hsp_len as f32;
    // overlap = (dxHspStart - lxHspEnd - 1) / shortestHsp
    // std::process::exit(-2);
    // log::debug!("Testing the overlap function:\nrightmost_hsp_start = {}\nleftmost_hsp_end = {}\nOverlap = {}",
    // rightmost_hsp_start,
    // leftmost_hsp_end,
    // overlap);

    if overlap <= OVERLAP_THR {
        return true
    }
    else { return false }

}


/// Compute the query/target coverage
fn compute_hsp_coverage(hsp_fragments: &Vec<HspCoordinates>) -> (u32, u32) {
    log::debug!("compute_hsp_coverage :: START
    Number of fragments:\t{}
    ", hsp_fragments.len());

    // Lenght of the segment between
    // the start of the leftmost HSP
    // and the end of the rightmost HSP
    // let mut stretched_coverage: u32 = 0;
    // Real coverage based on sum of HSPs lengths
    let mut coverage: u32 = 0;
    // auxiliary vectors to store start and end positions
    // let mut start_positions: Vec<u32> = Vec::new();
    // let mut end_positions: Vec<u32> = Vec::new();
    // start of the leftmost HSP
    let mut leftmost_hsp_start: u32 = u32::MAX;
    // end of the rightmost HSP
    let mut rightmost_hsp_end: u32 = 0;
    // let mut tmp_start: u32 = 0;
    // let mut tmp_end: u32 = 0;

    for fragment in hsp_fragments.iter() {
        // println!("{:#?}", fragment);
        coverage += fragment.end - fragment.start + 1;
        // println!("Updated coverage:\t{coverage}");
        // update leftmost start position
        if fragment.start < leftmost_hsp_start {
            leftmost_hsp_start = fragment.start;
            // println!("Updated leftmost start:\t{leftmost_hsp_start}");
        }
        // update rightmost end position
        if fragment.end > rightmost_hsp_end {
            rightmost_hsp_end = fragment.end;
            // println!("Updated rightmost end:\t{rightmost_hsp_end}");
        }
    }

    return (rightmost_hsp_end - leftmost_hsp_start + 1, coverage);

}



/// Parse Diamond results in BLAST-like tab-separated files with 7 fields
/// The output format is obtained using the following command (since Diamond ver 2) '-f 6 qseqid sseqid qstart qend sstart send bitscore'.
/// Default values:
/// minbiscore = 40
pub fn dmnd_parser(alnfile: &str, qseqlen_file: &Path, tseqlen_file: &Path, outpath: &Path, minbitscore: u64) {
    log::debug!("dmnd_parser :: START
    Aln file: {alnfile}
    File with query seq lengths: {:?}
    File with target seq lengths: {:?}
    Output name: {:?}
    Minimum bitscore:\t{minbitscore}
    ", qseqlen_file, tseqlen_file, outpath);

    /* NOTE: this is unefficient
    // load the sequence lengths from FASTA file
    let qid2len = load_protein_lengths(qseqlen_file);
    let mut tid2len: HashMap<String, u32>;
    // avoid loading the second one
    // if it is an interproteome alignment
    if qseqlen_file == tseqlen_file {
        // tid2len.clone_from(&qid2len);
        tid2len = qid2len.clone();
    }
    else {
        tid2len = load_protein_lengths(tseqlen_file);
    }
    */

    /*
    */
    // Make sure the paths are valid
    if !qseqlen_file.is_file() {
        panic!("The file with the query sequence lengths could not be found at\n{:?}", qseqlen_file);
    }
    if !tseqlen_file.is_file() {
        panic!("The file with the target sequence lengths could not be found at\n{:?}", qseqlen_file);
    }

    // Load sequence lengths from serialized file
    // NOTE: use JSON-serde to deserialize
    // Deserialize from the file
    let mut ifd = File::open(qseqlen_file).unwrap_or_else(|error| {
        panic!("Could not read the file {:?}", error);
    });

    // deserialize
    let qid2len: HashMap<String, u32> = serde_json::from_reader(ifd).unwrap();

    let tid2len: HashMap<String, u32>;
    // avoid loading the second one
    // if it is an interproteome alignment
    if qseqlen_file == tseqlen_file {
        // tid2len.clone_from(&qid2len);
        tid2len = qid2len.clone();
    }
    else {
        ifd = File::open(tseqlen_file).unwrap_or_else(|error| {
            panic!("Could not read the file {:?}", error);
        });
        tid2len = serde_json::from_reader(ifd).unwrap();
    }

    // println!("Query seqlen-cnt:\t{}\nTarget seqlen-cnt:\t{}\n", qid2len.len(), tid2len.len());

    /*
    // TODO: use serialized HashMaps instead of reading the files.
    log::debug!("Query protein lenghts:\t{}", qid2len.len());
    log::debug!("Target protein lenghts:\t{}", tid2len.len());
    log::debug!("Testing serialization...");
    std::process::exit(-5);
    */

    // Define the variables for the HSPs
    let mut qlen: u32;
    let mut slen: u32;
    let mut qstart: u32;
    let mut qend: u32;
    let mut sstart: u32;
    let mut send: u32;
    let mut bscore: f64;
    // Strings
    let mut qid: &str;
    let mut sid: &str;
    let mut tmpln = String::new();
    let mut flds: Vec<&str>;
    // let mut hitcnt: u64 = 0; // NOTE: debug only
    // To know when hits for a new query have been found
    let mut currenthit: (String, String);
    let mut prevhit: (String, String) = (String::new(), String::new());
    // Contains the HSPs for a given query
    // The key contains the HSP's start and end positions
    let mut queryhitsdict: IndexMap<(u32, u32), HspNumericFields> = IndexMap::new();
    // Contain HSP's numeric values
    let mut hsp_vals: HspNumericFields;

    // Exit if the file does not exist
    let fd = File::open(alnfile).unwrap_or_else(|error| {
        panic!("Problem opening the file {:?}", error);
    });

    // Create the output file
    /*
    let mut ofd = File::create("/home/salvocos/work_repos/rust-practice/data_and_test_files/test_parsing_files/TEST_OUTPUT.tsv").unwrap_or_else(|error| {
        panic!("Could not create the file {:?}", error);
    });
    */
    
    let mut ofd = File::create(outpath).unwrap_or_else(|error| {
        panic!("Could not create the file {:?}", error);
    });
    
    // std::process::exit(-5);

    // FIXME: this should be fixed in the input parameters
    let minbitscore = minbitscore as f64;

    // Create the buffer
    let mut reader = BufReader::new(fd);
    // Read the file
    while reader.read_line(&mut tmpln).unwrap() > 0 {

        flds = tmpln.splitn(7, '\t').collect();
        // hitcnt += 1;
        // println!("\nHit number:\t{hitcnt}");

        qid = flds[0];
        sid = flds[1];
        // extract values
        qstart = flds[2].parse::<u32>().unwrap() + 1;
        qend = flds[3].parse::<u32>().unwrap();
        sstart = flds[4].parse::<u32>().unwrap() + 1;
        send = flds[5].parse::<u32>().unwrap();
        bscore = flds[6].trim().parse::<f64>().unwrap();
        // Retrieve the lengths of query and target sequences
        qlen = qid2len[qid];
        slen = tid2len[sid];

        // Process an HSP if score abover threshold
        if bscore >= minbitscore {
            // println!("{}", tmpln.trim());
            // currenthit = (qid, sid);
            currenthit = (qid.to_string(), sid.to_string());
            // Store the numeric values
            // FIXME: qstart and qend would be the same as the tuple key of queryhitsdict
            // so they could technically be omitted from the struct...
            // but this is not a priority for now
            hsp_vals = HspNumericFields {qlen, slen, qstart, qend, sstart, send, bscore};

            if currenthit != prevhit {
                // log::debug!("We found a new hit!\nprevhit:\t{:?}\ncurrenthit:\t{:?}\n", {&prevhit}, currenthit);

                // NOTE: The "if not currentHitDict is None:" should start in here
                if !queryhitsdict.is_empty() {
                    // println!("Hits for query-target {:?}:\t{}", prevhit, queryhitsdict.len());

                    // Compute the scores for the query-target pair
                    // let to_write = extract_score_for_query_target_pair(&queryhitsdict, &prevhit.0, &prevhit.1);

                    writeln!(ofd, "{}", &extract_score_for_query_target_pair(&queryhitsdict, &prevhit.0, &prevhit.1).to_string())
                        .expect("The string should be written to the output file");

                    // NOTE: the execution is comparable to that of writeln!
                    /*
                    ofd.write_all(extract_score_for_query_target_pair(&queryhitsdict, &prevhit.0, &prevhit.1).to_string().as_bytes());
                    ofd.write_all("\n".as_bytes());
                    */
                    
                    // NOTE: Never use this way, it is too slow!
                    /*
                    writeln!(ofd, "{}\t{}\t{:.1}\t{}\t{}\t{}\t{}\t{}\t{}",
                    to_write.query_id,
                    to_write.target_id,
                    to_write.bscore,
                    to_write.query_len,
                    to_write.target_len,
                    to_write.tot_query_cov,
                    to_write.tot_target_cov,
                    to_write.query_cov,
                    to_write.target_cov,
                    );
                    */

                }
                // Empty the Hash
                queryhitsdict.clear();
                // Add the hsp to the HashMap
                queryhitsdict.insert((qstart, qend), hsp_vals);
                // update previous hit
                prevhit = currenthit;
                // prevhit = (currenthit.0, currenthit.1);
                // log::debug!("New value for prevhit:\t{:?}", prevhit);
            }

            else { // NOTE: debug only
                // println!("HSP for current query:\t{:?}", {currenthit});
                // println!("HSP stored in prevhit:\t{:?}", prevhit);
                log::debug!("Tuples are the same");
                // Simply add the HSP to the dictionary
                /* NOTE: this would cause HSP with same start-end to be overwritten
                queryhitsdict.insert((qstart, qend), hsp_vals);
                */

                // Make sure HSP with the same coordinates are not overwritten
                // This woud allow to use the HSP with the hishest score
                if !queryhitsdict.contains_key(&(qstart, qend)) {
                    queryhitsdict.insert((qstart, qend), hsp_vals);
                }
                // else {
                //     log::warn!("An HSP with positions {qstart}-{qend} is already in the HashMap.");
                // }
                // std::process::exit(1);
            }

        }

        // needs to be cleared because the content is appended to the buffer
        tmpln.clear();

    }

    // log::info!("\nProcessing the last line!");
    // Compute the scores for the query-target pair
    // let to_write = extract_score_for_query_target_pair(&queryhitsdict, &prevhit.0, &prevhit.1);
    // reuse the to_string functions
    // writeln!(ofd, "{}", &to_write.to_string());

    // NOTE: the above comments should be removed
    writeln!(ofd, "{}", &extract_score_for_query_target_pair(&queryhitsdict, &prevhit.0, &prevhit.1).to_string())
        .expect("The string should be written to the output file");

    // match &extract_score_for_query_target_pair(&queryhitsdict, &prevhit.0, &prevhit.1).to_string() {
    //     Err(e) => println!("{:?}", e),
    //     _ => writeln!(ofd, "{}", _)
    // }


    // NOTE: this is not necessary since the file would be closed after the function ends
    // drop(ofd);


}



/// Compute a single-line score for multiple HSPs from the same query
/// Extract the scores for 2 graph-nodes
fn extract_score_for_query_target_pair(queryhitsdict: &IndexMap<(u32, u32), HspNumericFields>, qid: &str, sid: &str) -> QueryTargetPairScores {
    log::debug!("extract_score_for_query_target_pair :: START
    Number of HSPs:\t{}
    Query:\t{}
    Target:\t{}
    ", queryhitsdict.len(), qid, sid);
    
    /*
    The scores a for a query-target pair should have the following format
    63363_O67184 63363_O66911 75.5 564 926 214 303 133 136
    The meaning of the fields is the following:
    col1: query
    col2: subject
    col3: sum( hsp_i.bitscore )
    col4: query length
    col5: subject length
    col6: max([hsp_1.qend, hsp_2.qend, ... hsp_N.qend]) - min([hsp_1.qstart, hsp_2.qstart, ... hsp_N.qstart] + 1)
    col7: max([hsp_1.send, hsp_2.send, ... hsp_N.send]) - min([hsp_1.sstart, hsp_2.sstart, ... hsp_N.sstart] + 1)
    NOTE: cols 6 and 7 contain the 'coverage' of the total sequence, disergarding
    regions of the seuquences that are not matching inbetween multiple HSPs.
    in the case of a single HSP cols 6 and 7 represent the acual sequence coverages
    col8: for i=[1, N], sum([hsp_i.qend - hsp_i.qstart] + 1)
    col9: for i=[1, N], sum([hsp_i.send - hsp_i.sstart] + 1)
    NOTE: cols 8 and 9 represent the actual coverages for the query and target respectively
    each entry in the dictionary has qstart and qend for the hsp as key
    and HspNumericFields structs containing the following information as values:
    qlen, slen, qstart, qend, sstart, send, bscore
    */

    // We will use structs instead
    // cdef int dxQuery, dxHit, lxQuery, lxHit
    // let mut extreme_query_coord: HspCoordinates;
    // let mut extreme_target_coord: HspCoordinates;
    // cdef int qlen, slen, qstart, qend, sstart, send, bitscore, fBitscore
    // cdef int overlapFound, i
    // cdef int fCol6, fCol7, fCol8, fCol9
    // fragments of HSPs represented by their start and end positions
    // NOTE: we structs instead of tuples for fragments
    // qFragmentList = [] #contain tuples with start and end positions of hsp on query
    // hFragmentList = [] #contain tuples with start and end positions of hsp on hit
    let mut accepted_qfragments: Vec<HspCoordinates> = vec![];
    let mut accepted_tfragments: Vec<HspCoordinates> = vec![];
    let mut overlap_found: bool;

    // This is used in two cases
    // 1) The dictionary contains only a single HSP
    // 2) to obtain the query and sequence lengths in the case of multi-hsps
    let single_hsp = Vec::from_iter(queryhitsdict.values())[0];

    // Initialize the fragments
    if queryhitsdict.len() == 1 {
        // log::warn!("SINGLE HSP:\n{:?}", single_hsp);
        // log::warn!("The query-target ({}-{}) only contains 1 HSP:\n{:?}\nThe score can be easily computed!",
        // qid,
        // sid,
        // single_hsp);

        // println!("Single HSP pair found ({qid}-{sid}).");

        //Create the final query-pair record and return it
        return QueryTargetPairScores {
            query_id: qid.to_string(),
            target_id: sid.to_string(),
            bscore: single_hsp.bscore,
            query_len: single_hsp.qlen,
            target_len: single_hsp.slen,
            tot_query_cov: single_hsp.qend - single_hsp.qstart + 1,
            tot_target_cov: single_hsp.send - single_hsp.sstart + 1,
            query_cov: single_hsp.qend - single_hsp.qstart + 1,
            target_cov: single_hsp.send - single_hsp.sstart + 1,
        }

    }
    else {
        let mut accepted_qfragments_cnt: usize = accepted_qfragments.len();
        // Aggregate score
        let mut aggr_score: f64 = 0.0;

        // for each HSP compute any overlap
        // for (query_coord_tpl, hsp_values) in queryhitsdict.iter() {
        for hsp_values in queryhitsdict.values() {
                // log::info!("\nProcessing at positions {:?}\nHSP:\t{:?}", query_coord_tpl, hsp_values);
            // This is the first fragment and should be accepted
            // if accepted_qfragments.len() == 0 {
            if accepted_qfragments_cnt == 0 {

                // Query HSP coordinates
                // NOTE: this is the same as using the info inside the struct
                // accepted_qfragments.push(HspCoordinates {start: query_coord_tpl.0, end: query_coord_tpl.1});
                accepted_qfragments.push(HspCoordinates { start: hsp_values.qstart, end: hsp_values.qend });
                accepted_tfragments.push(HspCoordinates { start: hsp_values.sstart, end: hsp_values.send });
                accepted_qfragments_cnt += 1;
                aggr_score += hsp_values.bscore;
            }
            // Check the for any overlaps among fragments
            else {
                // reset variable
                overlap_found = false;

                // Check overlaps for query positions
                for qfragment in accepted_qfragments.iter() {
                    // println!("Check overlaps in query HSP:\t{:?}", qfragment);

                    overlap_found = check_hsp_overlap(hsp_values.qstart, hsp_values.qend, qfragment.start, qfragment.end);
                    // check_hsp_overlap(hsp1start: u32, hsp1end: u32, hsp2start: u32, hsp2end: u32)

                    if overlap_found {
                        // log::info!("\nOverlap found for query fragments:\n({} - {})\n({} - {})",
                        // hsp_values.qstart,
                        // hsp_values.qend,
                        // qfragment.start,
                        // qfragment.end);

                        // log::debug!("\nExiting: found overlap on query HSPs.");
                        break;
                    }
                }

                    // Evaluate HSPs for target sequences
                if !overlap_found {
                    // iterate target HSPs
                    for tfragment in accepted_tfragments.iter() {
                        // println!("Check overlaps in target HSP:\t{:?}", tfragment);
                        overlap_found = check_hsp_overlap(hsp_values.sstart, hsp_values.send, tfragment.start, tfragment.end);
                        // Exit loop an overlap is found
                        if overlap_found {
                            // log::info!("\nOverlap found for target fragments:\n({} - {})\n({} - {})",
                            // hsp_values.sstart,
                            // hsp_values.send,
                            // tfragment.start,
                            // tfragment.end);
                            break;
                            }
                        }
                    }

                // Ignore the HSP if an overlap was found
                if overlap_found {
                    continue;
                }
                // accept the HSPs otherwise
                else {
                    accepted_qfragments.push(HspCoordinates { start: hsp_values.qstart, end: hsp_values.qend });
                    accepted_tfragments.push(HspCoordinates { start: hsp_values.sstart, end: hsp_values.send });
                    aggr_score += hsp_values.bscore;
                }

            }
        }

        // Compute coverages for query and target HSPs
        let (qcov_stretched, qcov) = compute_hsp_coverage(&accepted_qfragments);
        let (tcov_stretched, tcov) = compute_hsp_coverage(&accepted_tfragments);

        /*
        let x = QueryTargetPairScores {
            query_id: qid.to_string(),
            target_id: sid.to_string(),
            bscore: aggr_score,
            query_len: single_hsp.qlen,
            target_len: single_hsp.slen,
            tot_query_cov: qcov_stretched,
            tot_target_cov: tcov_stretched,
            query_cov: qcov,
            target_cov: tcov,
        };
        
        log::debug!("Testing calculation of coverages.");
        
        println!("{:#?}", x);
        
        // std::process::exit(-1);
        */

        return QueryTargetPairScores {
            query_id: qid.to_string(),
            target_id: sid.to_string(),
            bscore: aggr_score,
            query_len: single_hsp.qlen,
            target_len: single_hsp.slen,
            tot_query_cov: qcov_stretched,
            tot_target_cov: tcov_stretched,
            query_cov: qcov,
            target_cov: tcov,
        }

    }


}