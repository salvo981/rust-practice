/// This module contains functions related to FASTA sequence processing
use bio::io::fasta;
use std::fs::File;
use std::collections::HashMap;
use serde_json::to_writer_pretty;



/// Load proteins lengths
pub fn load_protein_lengths(filepath: &str) -> HashMap<String, u32> {

    // output hashmap
    let mut gene2len: HashMap<String, u32> = HashMap::new();

    // Exit if the file does not exist
    let fd = File::open(filepath).unwrap_or_else(|error| {
        panic!("Problem opening the file {:?}", error);
    });

    let fasta_reader = fasta::Reader::new(fd);
    let mut seqid: &str;
    let mut seqlen: u32;
    let mut proteome_size: u32 = 0;
    let mut proteins_cnt: u32 = 0;
    for result in fasta_reader.records() {
        let seqrec = result.expect("Error parsing the protein record.");
        seqid = seqrec.id();

        
        proteins_cnt += 1;
        // println!("{}", seqid);
        seqlen = seqrec.seq().len() as u32;

        // insert the id and length if not present
        if !gene2len.contains_key(seqid) {
            gene2len.insert(seqid.to_string(), seqlen);
        }
        else {
            panic!("The gene {seqid} is repeated in the FASTA file");
        }

        // The above could be done in one step
        // But this cannot check if the element is alredy present in he HashMap
        // gene2len.entry(seqid.to_string()).or_insert(seqlen);

        proteome_size += seqlen;
    }

    log::debug!("Sequences:\t{proteins_cnt}");
    log::debug!("Proteome size in aa:\t{proteome_size}");
    log::debug!("Genes loaded:\t{}", gene2len.len());

    
    // ################
    // Serialize the HashMap

    println!("{}", filepath);
    /*
    // Set the path to store the HashMap
    let gene2len_file_bincode_path = "/home/salvocos/work_repos/rust-practice/data_and_test_files/test_parsing_files/2.len.big.bincode";
    
    let mut ofd = File::create(gene2len_file_bincode_path).unwrap_or_else(|error| {
        panic!("Could not create the file {:?}", error);
    });
    
    bincode::serialize_into(ofd, &gene2len).unwrap();
    
    // Deserialize from the file
    let mut ifd = File::open(gene2len_file_bincode_path).unwrap_or_else(|error| {
        panic!("Could not read the file {:?}", error);
    });
    // deserialize
    let mut deserialized: HashMap<String, u32> = bincode::deserialize_from(ifd).unwrap();
    assert_eq!(deserialized, gene2len);
    // ### Bincode works correctly
    */
    
    // ################
    // NOTE: use serde_json
    let gene2len_file_serde_json_path = "/home/salvocos/work_repos/rust-practice/data_and_test_files/test_parsing_files/2.len.big.serde.json";
    let ofd = File::create(gene2len_file_serde_json_path).unwrap_or_else(|error| {
        panic!("Could not create the file {:?}", error);
    });
    
    // serde_json::to_writer(ofd, &gene2len).unwrap();
    serde_json::to_writer_pretty(ofd, &gene2len).unwrap();

    /*
    // Deserialize from the file
    ifd = File::open(gene2len_file_serde_json_path).unwrap_or_else(|error| {
        panic!("Could not read the file {:?}", error);
    });

    // deserialize
    let deserialized: HashMap<String, u32> = serde_json::from_reader(ifd).unwrap();

    assert_eq!(deserialized, gene2len);
    // ################
    */

    /*
    // NOTE: use pickles
    let gene2len_file_pickle_path = "/home/salvocos/work_repos/rust-practice/data_and_test_files/test_parsing_files/2.len.big.rust_pickle.pckl";
    ofd = File::create(gene2len_file_pickle_path).unwrap_or_else(|error| {
        panic!("Could not create the file {:?}", error);
    });
    let serial_gen2len_pckl = serde_pickle::to_vec(&gene2len, Default::default()).unwrap();
    ofd.write_all(&serial_gen2len_pckl).unwrap();
    
    // deserialize using pickles
    let mut ifd = File::open(gene2len_file_pickle_path).unwrap_or_else(|error| {
        panic!("Could not read the file {:?}", error);
    });

    let deserialized: HashMap<String, u32> = serde_pickle::from_reader(&ifd, Default::default()).unwrap();
    assert_eq!(deserialized, gene2len);
    // ### Pickle works correctly
    */

    return gene2len;

}