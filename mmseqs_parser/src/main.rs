/// This program implements a simple program
/// that parses tab-separated result files from MMseqs or Diamond


mod args_parser;
mod aln_file_parser;
mod seq_tools;

use std::path::Path;

use args_parser::Arguments;
use clap::Parser;

// use env_logger::Builder;
use env_logger::{Env, Builder, Target, WriteStyle};
use log::LevelFilter;


fn init_logger(loglev: LevelFilter) {
    // Env could be personalized further
    let env = Env::new();

    let mut builder = Builder::from_env(env);
    // builder.target(Target::Stderr);
    builder.filter_level(loglev);
    // Write the log level (e.g., warn, error etc.) in the log message
    builder.format_level(true);
    // Whether or not to write the module path in the default format.
    // builder.format_module_path(true);
    // Number of spaces to be used to indent mutiline logs messages
    builder.format_indent(Some(4));
    
    // Change the setting depending on the log level
    println!("level:\t{:?}", loglev);
    if LevelFilter::Debug == loglev {
        println!("level:\t{:?}", loglev);
        // Set the timestamp to use seconds
        builder.format_timestamp_secs();
        // Whether or not to write the target in the default format.
        builder.format_target(true);
    }
    else {
        // Do not show the timestamp
        builder.format_timestamp(None);
        // Whether or not to write the target in the default format.
        builder.format_target(false);
    }

    // Set a specific log level for each module
    // builder.filter_module("mmseqs::seq_tools", LevelFilter::Error);
    // Set where to write the log messages
    // By default it is Stderr
    // Pipe could be set to write into a file
    builder.target(Target::Stdout);
    // Decide if the style should printed (e.g. color based on the terminal/environment)
    // other options are Always and Never
    builder.write_style(WriteStyle::Auto);

    builder.init();

}


/* USE CUSTOM LOGGER
use log::{Record, Level, Metadata, LevelFilter, SetLoggerError};
use env_logger::{Builder, Target};
// use env_logger::{Logger, Env};

static CONSOLE_LOGGER: ConsoleLogger = ConsoleLogger;
struct ConsoleLogger;

impl log::Log for ConsoleLogger {
    fn enabled(&self, metadata: &Metadata) -> bool {
        metadata.level() <= Level::Trace
    }
    
    fn log(&self, record: &Record) {
        if self.enabled(record.metadata()) {
            println!("Rust says: {} - {} - {} - {} - {}", 
            record.level(),
            record.args(),
            record.line().unwrap(),
            record.file().unwrap(),
            record.module_path_static().unwrap(),
        );
    }
    }
    
    fn flush(&self) {}
}
*/


fn main() {

    /*
    // obtain the args from the CLI
    let args: Vec<String> = env::args().collect();
    // parse and obtain input parameters
    // let cfg: Config = parse_config(&args);
    let cfg: Config = Config::new(&args).unwrap_or_else(|err| {
        println!("Problem parsing arguments: {}", err);
        process::exit(1);
    });
    */
    

    
    /*
    // Use a custom logger
    log::set_logger(&CONSOLE_LOGGER).expect("Failed to initialize the logger.");
    // log::set_max_level(LevelFilter::Error);
    // log::set_max_level(LevelFilter::Warn);
    // log::set_max_level(LevelFilter::Info);
    // log::set_max_level(LevelFilter::Debug);
    log::set_max_level(LevelFilter::Trace);
    */

    // Let's parse the arguments using clap crate
    let args = Arguments::parse();
    println!("DEBUG ::\t{}", args.debug);

    // Set the logging environment
    let mut loglev: LevelFilter = LevelFilter::Info;
    if args.debug {
        loglev = LevelFilter::Debug;
    }
    // initialize the logger
    init_logger(loglev);

    // log::trace!("trace msg");
    // log::debug!("debug msg");
    // log::info!("info msg");
    // log::warn!("warning msg");
    // log::error!("error msg");
    // log::info!("Multi line\ninfo msg\nSecond Line!");

    println!("{:?}", args);
    println!("Input file: {:?}", args.input_path);
    println!("Output file: {:?}", args.output_path);
    println!("Minimum bitscore:\t{:?}", args.min_bitscore);
    println!("Diamond mode:\t{:?}", args.diamond);
    println!("Debug:\t{:?}", args.debug);
    
    // process_raw_aln_file_lnbyln_strings(filepath: &str)
    
    // aln_file_parser::process_raw_aln_file_lnbyln_strings(&args.input_path);
    
    // aln_file_parser::process_raw_aln_file_lnbyln_bytes(&args.input_path);
    
    // Process the FASTA
    // /home/salvocos/work_repos/rust-practice/data_and_test_files/test_parsing_files/2.mapped.big.fasta
    // let path22 = String::from("/home/salvocos/work_repos/rust-practice/data_and_test_files/test_parsing_files/2.mapped.big.fasta");
    
    // path to the serialized Hashmap
    let path22_serialized = String::from("/home/salvocos/work_repos/rust-practice/data_and_test_files/test_parsing_files/2.len.big.serde.json");

    
    // let qseqlen_path = Path::new(&path22_serialized);
    // Process the FASTA file
    let qseqlen_path= Path::new(&path22_serialized);
    let tseqlen_path= Path::new(&path22_serialized);
    
    
    // Output path
    let outpath = Path::new(&args.output_path);

    println!("{:?}", outpath);
    println!("{:?}", outpath.is_dir());

    if outpath.is_dir() {
        log::error!("The output path you provided is a directory.\n{:?}\nYou must also provide a file name.", outpath);
    }
    
    // TODO: make sure that the Path points to existing files
    aln_file_parser::dmnd_parser(&args.input_path, qseqlen_path, tseqlen_path, outpath, 40);
    
    
    // println!("DEBUG :: Main");
    // std::process::exit(1);
    /*
    
    // Handle the error case
    if let Err(e) = mmseqs_parser::run(cfg) {
        println!("Application error: {}", e);
        process::exit(1);
    }
    */

}

