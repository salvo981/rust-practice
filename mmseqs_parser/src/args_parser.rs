/// Parses the CLI arguments
use clap::Parser;

#[derive(Parser,Debug)]
#[clap(author, version, about)] // extract info from Cargo.toml
// #[command(name = "AlignmentScoreParser")]
// #[command(author = "Salvatore Cosentino <salvocos@gmail.com>")]
// #[command(version = "1.0")]
// #[command(about = "Parse alignment files", long_about = None)]
pub struct Arguments {
    /// Tab separated file with alignment scores from MMseqs2, Diamond or BLAST
    #[arg(short, long)]
    pub input_path: String,
    /// Output path in which to store the parsed alignment scores
    #[arg(short, long)]
    pub output_path: String,
    /// Minimum bitscore (alignments with lower bitscores are rejected)
    #[arg(short, long, default_value_t = 40)]
    pub min_bitscore: u64,
    /// Parse alignment file generate using Diamond
    #[arg(long)]
    pub diamond: bool,
    /// Debug mode (very verbose)
    #[arg(short, long)]
    pub debug: bool,
}
