/* 
    Examples of generic types, traits, and lifetimes
    from Chapters 10-11 of the Rust Book, "Generic Types, Traits, and Lifetimes"
*/


fn main() {
    println!("Hello, world!");
}
