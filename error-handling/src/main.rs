// Examples of error-handling from Chapter 9 of the Rust Book, "Error Handling"

use std::fs;
// use std::io::ErrorKind;
use std::io::{self, Read, ErrorKind};

/*
Example of error propagation
Listing 9-6
A function that reads a username from a ﬁle. If the ﬁle doesn’t exist or
can’t be read, this function will return those errors to the code that called the function.
*/
fn read_username_file() -> Result<String, io::Error> {
    // If Ok it T is a String with the username
    // If Err tnen io::Error is retuned
    let username_file_result = fs::File::open("./src/data/hello.txt");
    
    let mut username_file = match username_file_result {
        Ok(file) => file,
        Err(e) => return Err(e),
    };
    
    // username read from file
    let mut username = String::new();
    
    match username_file.read_to_string(&mut username) {
        Ok(_) => Ok(username),
        Err(e) => Err(e),
    }

}



/**
 Use the ? operator to simplify error propagation.
 Page 138, listing 9-7 of Rust book
*/
fn read_username_from_file() -> Result<String, io::Error> {
    // The '?' operator propagates the error time if it fails
    // if ok it return the File handler
    let mut username_file = fs::File::open("./src/data/hello.txt")?;
    // will contain the username
    let mut username = String::new();
    // Read the username from the file
    username_file.read_to_string(&mut username)?;
    Ok(username)
}



/**
 Chain calls using the ? operator to simplify error propagation.
 Page 138, listing 9-8 of Rust book
*/
fn read_username_from_file_chained() -> Result<String, io::Error> {
    // Concatenate calls that use the '?' operator  
    let mut username = String::new();
    // Chain the calls that make use of ?
    fs::File::open("./src/data/hello.txt")?.read_to_string(&mut username)?;
    Ok(username)
}



/**
 Using the standars library is even simpler!
 Page 139, listing 9-9 of Rust book
*/
fn read_username_from_file_fs() -> Result<String, io::Error> {
    return fs::read_to_string("./src/data/hello.txt");
}


/**
 Example of use of ? with Option enum!
 Page 140, listing 9-11 of Rust book
*/
fn last_char_of_first_line(text: &str) -> Option<char> {
    // Return the last char of the first line
    // next() and last() both return Option enum types
    // next() is none if there are no lines
    // last() is none if the iterator is empty (the first line is empty)
    text.lines().next()?.chars().last()
}



fn main() {
    // panic!("Crash and burn");

    /*
    // example that let rust panic!
    // inxed-out-of-bounds
    let v = vec![1, 2, 3];

    v[3];
    */

    // example of call that might fail
    // and hence returns a Result object
    let greeting_file_result = fs::File::open("hello.txt");

    /*
    In this case the code will panic! regardless of the reason File::open failed
    // Let check if there was an error using match
    let greeting_file = match greeting_file_result {
        Ok(file) => file,
        Err(error) => panic!("Problem opening the file: {:?}", error),
    };
    */

    /*
    // Listing 9-5
    // Let handle the case in which the file does not exist
    // creating a new file and returning the handle instead of panicking
    let greeting_file = match greeting_file_result {
        Ok(file) => file,
        // handle the case in which the file does not exist
        Err(error) => match error.kind() {
            // Create a new file
            ErrorKind::NotFound => match File::create("hello.txt") {
            Ok(fc) => fc,
            Err(e) => panic!("Problem creting the file: {:?}", e),
            },
            other_error => {
                panic!("Problem opening the file: {:?}", other_error);
            }
        },
    };
    */
    
    /*
    Using match works, but can be verbose and convoluted.
    A more concise way to hanlde error cases is throuvh closures (exmplained in chapter 13)
    */
    // The below does the same as above (listing 9-5)
    // but uses closures, which makes easier to read compared to using match.
    /*
    let greeting_file = File::open("hello.txt").unwrap_or_else(|error| {
        if error.kind() == ErrorKind::NotFound {
            File::create("hello.txt").unwrap_or_else(|error| {
                panic!("Problem creating the file: {:?}", error);
            })
        }
        else {
            panic!("Problem opening the file: {:?}", error);
            }
    }); // this closes the first unwrap_or_else
    */
    
    /*
    It is also possible to use the unwrap method from the Result enum
    let greeting_file = File::open("hello.txt").unwrap();
    */

    /*
    Using expect we can provide a better message for the panic! macro
    */
    let greeting_file = fs::File::open("./src/data/hello.txt")
        .expect("The file was not found");

    /*
    Error propagation
    For example, Listing 9-6 shows a function that reads a username from a ﬁle. If the ﬁle doesn’t exist or
    can’t be read, this function will return those errors to the code that called the function.
    */

    // Propagate the error using the ? operator
    let mut read_user = read_username_from_file();

    println!("My user is {:?}", read_user);

    read_user = read_username_from_file_chained();
    println!("My user is {:?}", read_user);

    read_user = read_username_from_file_fs();
    println!("My user is {:?}", read_user);


}
