fn loop_no_return(x: i32) -> i32 {
    let mut cnt = 0;

    loop {
        cnt += 1;

        if cnt == x {
            break;
        }

    }

    cnt

}



fn loop_with_return(x: i32) -> i32 {
    let mut cnt = 0;

    let result: i32 = loop {
        cnt += 1;

        if cnt == x {
            break cnt * 2;
        }

    };

    result

}

fn print_mesurement(x: i32, unit_label: char) {
    println!("The mesurement is {}{}.", x, unit_label);
}

fn six() -> i32 {
    6
}

fn main() {
    print_mesurement(10, 'm');

    println!("The value from function six is {}", six());

    let iter_limit: i32 = 5;

    println!("Loop with return value returned {}", loop_with_return(iter_limit));
    println!("Loop with return value returned {}", loop_no_return(iter_limit));

}
