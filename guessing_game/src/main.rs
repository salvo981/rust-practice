use std::io;
use std::cmp::Ordering;
use rand::Rng;


fn main() {
    println!("Indovina il numero!");

    let maxerr: u16 = 4;
    let mut errcnt: u16 = 0;

    // generat and print the random number
    // let secret_number: u32 = rand::thread_rng().gen_range(1..101);
    
    loop {
        println!("Please input your guess.");
        let mut guess = String::new();

        // get the input from the user
        io::stdin()
        .read_line(&mut guess)
        .expect("Failed to read the line");
        // convet to integer
        // let guess: u32 = guess.trim().parse().expect("Please input a valid number!");

        let guess: u32 = match guess.trim().parse() {
            Ok(num) => num,
            Err(_) => continue,
        };

        // Use the input
        println!("You guessed {}", guess);
        
        let secret_number: u32 = rand::thread_rng().gen_range(1..101);
        // compare to guess to the secret number
        match guess.cmp(&secret_number) {
            Ordering::Less => {
                println!("Too small!");
                errcnt += 1;
                if errcnt == maxerr {
                    println!("Max number of errors reached!");
                    break;
                }
            }
            Ordering::Greater => {
                println!("Too big!");
                errcnt += 1;
                if errcnt == maxerr {
                    println!("Max number of errors reached!");
                    break;
                }
            }
            Ordering::Equal => {
                println!("You win!");
                break;
            }
        }

        println!("The secret number is {}", secret_number);
    }

}
