/*
Example of libray creation
It from chaper 7 of Rust programming book
The example describes what it required to manage a restaurant
*/

// In the front house of the restauant seating, ordering, eating, serving etc. is done
mod front_house {
    
    // example of parent function
    fn parent_is_front_house() {}
    
    pub mod hosting {
        pub fn add_to_waitlist() {}
        
        fn seat_at_table() {}
        
        // call function using a relative path and the super keyword
        pub fn test_super_to_call() { super::parent_is_front_house() }
    }
    
    mod serving {
        fn take_order() {}
        
        fn serve_order() {}
        
        fn take_payment() {}
        
        // crate::front_house::parent_fn();
    }
}

// we can use the 'use' keyword to define paths, which is more convenient
use crate::front_house::hosting;
use crate::front_house::hosting::add_to_waitlist;

// Define a function to show how to use relative and abosulte paths to items in modules
pub fn eat_at_restaurant() {
    // Absolute path
    crate::front_house::hosting::add_to_waitlist();
    // It looks better if we import the path through the use keyword
    hosting::add_to_waitlist();
    // And this those the same as the other 2 lines above
    add_to_waitlist();
        
    // Relative path
    front_house::hosting::add_to_waitlist();
    
    // call a function with the relative path,
    // which calls a function in the parent of the calling function
    front_house::hosting::test_super_to_call();
    
}