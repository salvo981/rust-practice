//practice using enum and match (chapter 6 of Rust book)

// Example to enumerate the types of IP numbers

// This only defines the type but no data is associated to it 
#[derive(Debug)]
enum IpAddrKind {
    V4,
    V6,
}

/*
Enum can also have values associated. This can be useful for example, to avoid creqting a
separate struct which has enum types as parameters.
In the example below values are associated to the enum types to represent the IP address for each type
*/
enum IpAddr {
    V4(u8, u8, u8, u8),
    V6(String),
}

/*
Enums can be seen as a way to include differnt struct inside a single object (an enum)
This way we could use, for examplea a single enum to pass it to a function,
while using struct we would need to pass a single struct type for each of the enum variants
*/
// This is a an example of more complicated enum
enum Message {
    Quit,
    Move {x: i32, y: i32},
    Write(String),
    ChangeColor(i32, i32, i32),
}

// Using structs we would need 4 seperate structs
/*
struct QuitMsg; // unit struct
struct MoveMsg {
    x: i32,
    y: i32,
}
struct WriteMsg(String); // Tuple struct
struct ChangeColorMsg(i32, i32, i32); //Tuple struct
*/

// Like with structs it is possible to define methods using impl
impl Message {
    fn call(&self) {
        // body of the function
    }
}

#[derive(Debug)]
enum UsState {
    Alabama,
    Alasca,
    California,
    // and all the other states...
}

// Enum used in example of control flow (Chapter 6)
enum Coin {
    Penny,
    Nickel,
    Dime,
    Quarter(UsState),
}

// Function that perform the control flow using match based on the variant of the coin enum
fn value_in_cents(coin: Coin) -> u8 {
    match coin {
        Coin::Penny => {
            println!("Your coin is a Penny which is 0,01 dollars.");
            1 // This is returned, as it is the last line in the block of curly brackets
        }
        Coin::Nickel => 5,
        Coin::Dime => 10,
        Coin::Quarter(state) => {
            println!("Your coin is a quarter (0.25 dollars) from the {:#?} state.", state);
            25
        }
    }
}

// patter matching for unknonw coin
fn count_coins_with_match(coin: &Coin, mut coincount: u32) -> u32 {
    match coin {
        Coin::Quarter(state) => {
            println!("The state of this quarter is {:#?}!", state);
            coincount
            },
        _ => {
            coincount += 1;
            println!("The number of non-quarter coins is now {}", coincount);
            coincount
            }
    }
}

// patter matching for unknonw coin
// Same as above but using if let
fn count_coins_with_if_let(coin: &Coin, mut coincount: u32) -> u32 {
    if let Coin::Quarter(state) =  coin {
        println!("The state of this quarter is {:#?}!", state);
        coincount
    } else {
        coincount += 1;
        println!("The number of non-quarter coins is now {}", coincount);
        coincount
        }
}





// Try to use match with the Option enum
fn plus_one(x: Option<i32>) -> Option<i32> {
    match x {
        None => None,
        Some(i) => Some(i + 1),
    }
}

/*
Dummy functions for the catch-all examples
*/
fn add_fancy_hat() {}
fn remove_fancy_hat() {}
fn move_player(num_spaces: u8) {}
fn reroll() {}


fn main() {
    println!("Practice enum!");
    
    // Initialize an enum
    let ip_type4 = IpAddrKind::V4;
    
    println!("Your IP is of type {:?}", ip_type4);
    
    // initialize and print enums of IP addresses
    let home = IpAddr::V4(0, 10, 20, 125);
    let _loopback = IpAddr::V6(String::from("::1"));
    
    // Define a Message enum and do something with it
    let m: Message = Message::Write(String::from("I like soba"));
    m.call();

    // Let's define a coin and identify it using the value_in_cents function
    let mypenny: Coin = Coin::Penny;
    value_in_cents(mypenny);
    
    // Let's not try to create and match a quarter of dollar
    let myquarter = Coin::Quarter(UsState::California);
    value_in_cents(myquarter);
    
    // Let's use the plus_one function
    let five = Some(5);
    let six = plus_one(five);
    let none = plus_one(None);
    
    // Example of catch-all in which we use (bind to) the variable
    // Code in chapter 6 "Catch-all patters and _ Placeholder"
    let dice_roll: u8 = 9;
    // Perform the match using a catch-all
    match dice_roll {
        3 => add_fancy_hat(),
        7 => remove_fancy_hat(),
        other => move_player(other) // in this case we use the value of dice_roll
    }

    // Example of catch-all in which we DO NOT use (bind to) the variable
    // Code in chapter 6 "Catch-all patters and _ Placeholder"
    // Perform the match using the _ placeholder
    match dice_roll {
        3 => add_fancy_hat(),
        7 => remove_fancy_hat(),
        _ => reroll() // catch-all in which we do not use the value (of dice_roll in this case)
    }
    
    // Example of use of 'if let' as an alternative to simple match (e.g., catch-all, or with only two patter matches)
    // Let's use if let and else to output the state of Quarter coin and count all the other types of coins
    let mut count: u32 = 0;
    let quartercoin = Coin::Quarter(UsState::Alabama);
    let pennycoin: Coin = Coin::Penny;

    // Match the coin pattern and update the counts
    count = count_coins_with_match(&pennycoin, count);
    count = count_coins_with_match(&quartercoin, count);

    println!("Coin count is now {}!", count);
    
    // Match the coin pattern and update the counts
    // using 'if let'
    count = count_coins_with_if_let(&pennycoin, count);
    count = count_coins_with_if_let(&quartercoin, count);
    count = count_coins_with_if_let(&pennycoin, count);
    println!("Coin count is now {}!", count);
    /*
    match coin {
        Coin::Quarter(state) => println!("The state of this quarter is {:#?}!", state),
        _ => count += 1,
    }
    */
            
            
            
}
