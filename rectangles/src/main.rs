#[derive(Debug)]
struct Rectangle{
    width: u32,
    height: u32
}

// define methods ofr the Rectangle struct
impl Rectangle{
    fn area(&self) -> u32 {
        self.width * self.height
    }
    
    // function that checks if another rectangle r2 fits
    // inside self (the rectangle calling the function)
    fn can_hold(&self, rect2: &Rectangle) -> bool {
        (self.width > rect2.width) && (self.height > rect2.height)
    }
    
}

fn main() {
    let width1: u32 = 30;
    let height1: u32 = 50;

    println!(
        "The area of the rectangle is {} square pixels.",
        area(width1, height1)
    );

    // Refacor using tuples
    let rect1: (u32, u32) = (30, 50);

    println!(
        "The area of the rectangle [using tuples] is {} square pixels.",
        area_tpl(rect1)
    );

    // Create the struct
    let rect_struct = Rectangle {
        width: 30,
        height: 50,
    };

    println!(
        "The rectangle is {:#?} and its area is {} square pixels.",
        rect_struct, area_struct(&rect_struct)
    );

    // Compute the area using the method for the Rectangle struct
    println!(
        "\nUse the Struct method:\nThe rectangle is {:#?} and its area is {} square pixels.",
        rect_struct, rect_struct.area()
    );

    // Define a second rectangle and see if it fits the inside the first one
    let rect2 = Rectangle {
        width: 20,
        height: 45,
    };

    // Compute the area using the method for the Rectangle struct
    println!(
        "\nPrint the two rectangles:\nRectangle 1 is {:#?}\n with area {} of pixels\n
         and Rectangle 2 is\n{:#?}\n with area {} of pixels\n",
        rect_struct, rect_struct.area(), rect2, rect2.area()
    );

    let r2FitsR1: bool = rect_struct.can_hold(&rect2);

    // Print some output
    if r2FitsR1{
        println!("R2 fits in R1");
    }
    else {
        println!("R2 DOES NOT fits in R1");
    }

    
}



fn area(width: u32, height: u32) -> u32 { 
    width * height
}

// Area function that makes use of Tuples
fn area_tpl(dimensions: (u32, u32)) -> u32 {
    dimensions.0 * dimensions.1
}

// Area function making use of structs
fn area_struct(rect: &Rectangle) -> u32 {
    rect.width * rect.height
}