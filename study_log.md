#### Friday 13 January 2023  
  
  Read again about ownership and borrowing in `Chapter 4`
  
  Restart from Page 70 (`Chapter 5`)

#### Tuesday 17 January 2023  

  Read about Structs and Tuples (`Chapter 5`) pp 70-80
  

#### Wed. 18 January 2023  

  Read about Structs `impl` defined methods pp 80-83
  implemented some simple examples
  

#### Thu. 19 January 2023

> pgg 85-96
- enum intro
- Study the `Option` enum
- pattern matching on enum using `match`
- pattern matching on enum using `if let` 
   
#### Fri. 20 January 2023

> pgg 97-114
- Modules Cheat sheet: simple example called `backyard`
- Use of absolute and relative paths
- Use of pub, super keywords (restaurant example, chapter 7)
- import using the `use` keyword
- use of `pub use` combination for wider-scope imports
- separate modules into different files in different directories

#### 23-24 January 2023
> pgg 114-129
- Collections
- - Vectors
- - Strings
- - HashMap

#### 16 February 2023
> pgg 130-133
- Error Handling
- - Recoverable VS unrecoverable errors

#### 17 February 2023
> pgg 134-138
- Recoverable Errors with Result
- - Recoverable Errors with Result
- - Alternatives to Using match with Result<T, E>
- - Shortcuts for Panic on Error: unwrap and expect
- - Propagating Errors

#### 24 March 2023
> pgg 138-145
- Recoverable Errors with Result
- - Use of the ? operation for error propagation
- - Example of use of ? with `Result` and `Option` enum
- To panic! or Not to panic!
- - Example of when to panic! or use Result
- - Guidelines for error handling
- - `IMPORTANT:` Create custom types for Type validation.
- - `IMPORTANT:` listing 9-13

Restart from page 146 (Generic Types, Traits, and Lifetimes)

> TODO
> Some exercises about Vectors, Strings and HashMap...
> E.g. consider using the exercise book
