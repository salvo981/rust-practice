/*
Example of code organization in modules.
From Rust programming book V162.
Chapter 7
*/

// Import the Asparagus struct
use crate::garden::vegetables::Asparagus;

pub mod garden; // include the code in garden.rs

fn main() {
    println!("Let's play with modules!");
    let plant = Asparagus {};
}