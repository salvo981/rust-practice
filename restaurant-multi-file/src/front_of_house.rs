// In the front house of the restauant seating, ordering, eating, serving etc. is done
pub mod hosting;
    
mod serving {
    fn take_order() {}
    
    fn serve_order() {}
    
    fn take_payment() {}
}
