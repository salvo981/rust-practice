/*
Example of libray creation, in which modules are separated in mutiple files
It is from chaper 7 of Rust programming book
The toy example is about managing a restaurant
*/


mod front_of_house;

pub use crate::front_of_house::hosting;

/*
pub fn eat_at_restaurant() {
    hosting::add_to_waitlist();
}
*/

// Define a function to show how to use relative and abosulte paths to items in modules
pub fn eat_at_restaurant() {
    // Absolute path
    crate::front_of_house::hosting::add_to_waitlist();
    // front_house::hosting::add_to_waitlist();
    // It looks better if we import the path through the use keyword
    hosting::add_to_waitlist();
    // And this those the same as the other 2 lines above
    // add_to_waitlist();
        
    
}