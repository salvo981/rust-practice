/*
This program implements a simple grep program
that searchs for a word inside of file
*/

// This is the content of the example file
/*
Il mi o amico si chiamava Mario.
Mario erea molto stupido e faceva sempre l'idiota. Non so come facesse, ma era sempre molto bravo a rompere i coglioni a tutti.
Che coglione Mario!
*/

use std::env;
// We use this to allow exiting without panicking
use std::process;
// import the code to parse CLI arguments
// use minigrep::{Config, run};
use minigrep::Config;



fn main() {

    // obtain the args from the CLI
    let args: Vec<String> = env::args().collect();
    // parse and obtain input parameters
    // let cfg: Config = parse_config(&args);
    let cfg: Config = Config::new(&args).unwrap_or_else(|err| {
        println!("Problem parsing arguments: {}", err);
        process::exit(1);
    });

    // println!("{:?}", cfg);
    println!("Searching for:\t{}", cfg.query);
    println!("In file: {}", cfg.filename);

    // let _ = run(cfg);

    // Handle the error case
    if let Err(e) = minigrep::run(cfg) {
        println!("Application error: {}", e);
        process::exit(1);
    }

}

