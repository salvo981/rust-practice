use std::error::Error;
use std::fs;



pub struct Config {
    pub query: String,
    pub filename: String,
}

impl Config {
    // parse CLI arguments and create Config struct
    pub fn new(args: &Vec<String>) -> Result<Config, &str> {
        // Make sure at least 3 paramters are provided
        if args.len() < 3 {
            //Do something in here
            return Err("Not enough arguments.");
        }
        // fn parse_config(args: &[String]) -> (&str, &str) {
        // Get the command line parameters
        // let query = &args[1];
        // let filename = &args[2];
        // It should be avoided to use clone, as it is not efficient
        // Pass references instead (as done above)
        let query = args[1].clone();
        let filename = args[2].clone();
        
        Ok(Config {query, filename})
    }
}



pub fn run(cfg: Config) -> Result<(), Box<dyn Error>> {
    // read the content of the file
    
    // This would panic if the input could not be read
    // let contents = fs::read_to_string(cfg. filename).expect("The input file could not be read.");
    // Let's instead unwrap the Result or return an error
    let contents = fs::read_to_string(cfg.filename)?;
    // expect("The input file could not be read.");
    println!("Raw print of the input text file:\n{contents}");
    // Search for the query string
    // let results = search(&cfg.query, &contents);

    let mut matches: u32 = 0;
    for ln in search(&cfg.query, &contents) {
        matches += 1;
        println!("Match ({matches}):\t{ln}");
    }

    print!("Total matches:\t{matches}");

    Ok(())

}


///Search a query inside a vector of strings
/// # Examples
/// ```
/// let query: &str = "due";
/// let contents: &str = "Whatever fine for now...\n\
/// Let's say\n\
/// 'Uno, due, tre.'";
/// ```
pub fn search<'a>(query: &str, contents: &'a str) -> Vec<&'a str> {
    // Output vector
    let mut results: Vec<&str> = vec![];
    // search the content for the query term
    for ln in contents.lines() {
        if ln.contains(query) {
            println!("\n{ln}");
            results.push(ln);
        }
    }

    results
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn case_sensitive() {
        let query: &str = "due";
        let contents: &str = "Whatever fine for now...\n\
        Let's say\n\
        'Uno, due, tre.'";

        assert_eq!(vec!["'Uno, due, tre.'"], search(query, contents));
    }

    #[test]
    fn case_insensitive() {
        let query: &str = "rUsT";
        let contents: &str = "Rust:\n\
        Pick three\n\
        Trust me.";

        assert_eq!(vec!["Rust:", "Trust me."], search(query, contents));

    }
}