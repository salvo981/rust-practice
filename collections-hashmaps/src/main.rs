/*
Examples of HashMaps, which are collections as in chaper 8 of Rust programming book.
*/

use std::collections::HashMap;


fn main() {
    println!("Playing with HashMaps!");
    
    // Create HashMap
    let mut scores: HashMap<String, u32> = HashMap::new();
    let favourite_team: String = String::from("Minchia");
    // insert some values
    scores.insert(favourite_team, 100);
    scores.insert(String::from("Inter"), 25);
    scores.insert(String::from("Palermo"), 45);
    
    //accessing values in HashMaps using get
    // NOTE: the get method returns an Option<&V> object,
    // so we could handle the extraction using a match or 'if let'
    // let score = scores.get(&"Inter".to_string().copied().unwrap_or(0));
    let team_name:String = String::from("Inter");
    /*
    This program handles the Option by:
    - calling copied to get an Option<i32>, rather than an Option<&i32>
    - then unwrap_or to set score to zero if scores doesn't have an entry for the key.
    - the returned value is an u32 regardless if Option(V) is None
    */
    let score = scores.get(&team_name).copied().unwrap_or(0);
    
    /*
    Let's use a match to handle the expression
    NOTE: the one-liner is way easier to do
    // let score: Option<&u32> = scores.get(&team_name);
    let score: Option<u32> = scores.get(&team_name).copied();
    let mut team_score: u32 = 0;
    // Let's extract the value using an if let
    match score {
        Some(tmp_score) => team_score = tmp_score,
        None => team_score = 0,
    }
    */
    
    println!("Team score for {team_name} is {score}");
    
    // Let's extract the value using an if let
    /*
    // if let Coin::Quarter(state) =  coin {    
    if let Some() =  score {
        println!("The score is {myscore}");
    } else {
        score = 0;
    }
    */
    
    /*
    Iterate through key/value pairs in HashMaps
    */
    
    // Iterate key/value pairs
    for (key, value) in &scores {
        println!("{key}:\t{value}");
    }
    
    // Type that do not implement the copy trait (e.g., String)
    // Will be owned by the HashMap, and not usable externally once inserted
    // For example:
    let field_name = String::from("Favorite color");
    let field_value = String::from("Blue");
    let mut map = HashMap::new();
    map.insert(field_name, field_value);
    // field_name and field_value are invalid at this point, try using them and
    // see what compiler error you get!
    // println!("field_name:\t{field_name}\nfield_value:\t{field_value}\n");
    // It would OK if we would use types implementing the copy trait
    
    /*
    Updating a Hash Map
    */
    
    /*
    Overwriting a value
    */
    let mut scores = HashMap::new();
    scores.insert(String::from("Blue"), 10);
    // This would simply overwrite the value
    scores.insert(String::from("Blue"), 25);
    println!("{:?}", scores);
    
    /*
    Check exstance of key or value, and update or add as required
    We use entry API to check the existance of a value
    */
    let mut scores = HashMap::new();
    scores.insert(String::from("Blue"), 10);
    // Check if Yellow exists
    // Entry returns an enum called Entry, which tell has wether there is a value associated to the key
    // or_insert is a method of the Entry enum,
    // or_insert: inserts a default value V (i32 in this case) if the key is not present
    scores.entry(String::from("Yellow")).or_insert(50);
    // Because Blue is already in the HashMap, then nothing is modified 
    let last_score = *scores.entry(String::from("Blue")).or_insert(50);
    println!("{:?}", scores);
    println!("The extracted score is {last_score}");
    
    /*
    Updating a Value Based on the Old Value
    */
    
    /*
    Another common use case for hash maps is to look up a key’s value and then update it based on the old value.
    Example: From Listing 8-25
    Problem: Counts how many times each word appears in some text.
    Our Hash map is of type HashMap<String, u32> 
    - Keys are words from the text
    - Everytime we find the same word, we increment the associated counter
    - If it’s the ﬁrst time we’ve seen a word, we’ll ﬁrst insert the value 0.
    */
    let text = "hello world wonderful world";
    let mut wcnt = HashMap::new();
    
    // Iterate the string splitting at each blank character
    for word in text.split_whitespace() {
        wcnt.entry(word)
            .and_modify(|cnt| {*cnt += 1})
            .or_insert(1);
    }

    println!("\nWord occurencies in\n'{text}' [using and_modify from Entry]\n");
    println!("{:?}", wcnt);
    
    // Do the same thing without using and_modify
    let mut wcnt: HashMap<&str, u32> = HashMap::new();
    // let mut wcnt: HashMap<&str, u32> = HashMap::new();

    // println!("{text}");
    
    for word in text.split_whitespace() {
        // cnt contains the current count which can 1 or a higher value
        let cnt = wcnt.entry(word).or_insert(0);
        // We need to update the counter
        // with * we point to the portion of memory to which cnt refers
        // and we modify it
        *cnt += 1;
        // This is same as the above
        // *cnt = *cnt + 1;
    }
    
    println!("\nWord occurencies in\n'{text}' [without and_modify]\n");
    println!("{:?}", wcnt);
    
}
