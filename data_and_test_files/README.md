# test files used for practicing RUST

#### test_parsing_files.tar.xz

Contains example reference files of RAW and parsed alignment files generated using SP2 v2.0.5a3.  
These files are used as reference to which the parsing results for the program in `mmseqs_parser`.  

