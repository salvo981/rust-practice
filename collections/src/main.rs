/*
Examples of use of collections, chaper 8 of Rust programming book
*/

// enum to be used in a vector
enum SpreadsheetCell {
    Int(i32),
    Float(f64),
    Text(String),
}


fn main() {
    println!("Let's play with collections!");
    
    /*
    Vectors
    */

    // create a empty vector
    // NOTE: we specified the type, because the vector is empty anf Rust could not infer the type
    // let v: Vec<i32> = Vec::new();
    // Create a non-empty vector
    // in this case Rust will set type to i32 as it is the default type
    let mut v = vec![1, 2, 3];

    // let's add some values
    v.push(4);
    v.push(5);

    // Let's read the values in the vector (using referencing)
    let third: &i32 = &v[2];
    println!("The third element in the vector (read using referencing) is {}", third);
    
    // A different way to access the values is by using the get function
    // the get function returns an Option<T> value we can use with match
    let third: Option<&i32> = v.get(3);
    
    match third {
        Some(third) => println!("The third element in the vector (using the get function) is {}", third),
        None => println!("There is not third element in the vector."),
    }
    
    // iterate the vector only for reading
    for i in &v {
        println!("Value in v (read only):\t{i}");
    }
    
    // iterate and print the elements
    // IMPORTANT: we can modify each value in the vector,
    // but we cannot add or remove elements, because vector values are contiguous in memory
    println!("\nIterate and modify the values.");
    for i in &mut v {
        println!("old:\t{i}");
        // *i += 2;
        *i = *i * *i;
        println!("updated:\t{i}");
    }
    
    /*
    Create a vector that can hold different types using a enum
    NOTE: by default a vector contains elements of the same type
    we can use enum to circumvent this limitation
    */
    
    /* initialize a row of the spreadsheet using SpreadSheetCell
    */
    let row = vec![
        SpreadsheetCell::Int(3),
        SpreadsheetCell::Float(0.5),
        SpreadsheetCell::Text(String::from("Minchia")),
    ];
    
}
